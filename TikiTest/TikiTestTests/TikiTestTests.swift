//
//  TikiTestTests.swift
//  TikiTestTests
//
//  Created by THAI LE QUANG on 7/19/18.
//  Copyright © 2018 THAI LE QUANG. All rights reserved.
//

import XCTest
@testable import TikiTest

class TikiTestTests: XCTestCase {
    
    var viewController: ViewController!
    
    override func setUp() {
        super.setUp()
        
        viewController = ViewController.controllerFromStoryboard()!
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testTweet() {
        let _ = viewController.view
        
        var string = "The quick brown fox jumps over the lazy dog"
        var check = viewController.checkValidTweet(with: string)
        XCTAssertTrue(check, "Tweet is invalid. Tweet must be not a span of nonwhite space character and lenght bigger than 50")
        
        var array = viewController.splitTweet(with: string)
        var arrayCount = array.count
        for sub in array {
            XCTAssertLessThan(sub.count, 51, "Sub tweet must has lenght less than 50")
        }
        
        string = "The quick brown fox jumps over the lazy dog The quick brown fox jumps over the lazy dog"
        check = viewController.checkValidTweet(with: string)
        XCTAssertTrue(check, "Tweet is invalid. Tweet must be not a span of nonwhite space character and lenght bigger than 50")
        
        array = viewController.splitTweet(with: string)
        arrayCount = array.count
        for sub in array {
            XCTAssertLessThan(sub.count, 51, "Sub tweet must has lenght must not more than 51")
        }
        
        string = "ThequickbrownfoxjumpsoverthelazydogThequickbrownfoxjumpsoverthelazydog"
        check = viewController.checkValidTweet(with: string)
        XCTAssertFalse(check, "Tweet is invalid. Tweet must be not a span of nonwhite space character and lenght bigger than 50")
        
        string = "The quick brown fox jumps over the lazy dog 🐶🐮🐶🐮 The quick brown fox jumps over the lazy dog"
        check = viewController.checkValidTweet(with: string)
        XCTAssertTrue(check, "Tweet is invalid. Tweet must be not a span of nonwhite space character and lenght bigger than 50")
        
        array = viewController.splitTweet(with: string)
        arrayCount = array.count
        for sub in array {
            XCTAssertLessThan(sub.count, 51, "Sub tweet must has lenght must not more than 50")
        }
        
        string = "The quick brown fox 🐶🐮🐶🐮 jumps over the lazy dog The quick brown fox jumps over the lazy dog"
        check = viewController.checkValidTweet(with: string)
        XCTAssertTrue(check, "Tweet is invalid. Tweet must be not a span of nonwhite space character and lenght bigger than 50")
        
        array = viewController.splitTweet(with: string)
        arrayCount = array.count
        for sub in array {
            XCTAssertLessThan(sub.count, 51, "Sub tweet must has lenght must not more than 50")
        }
        
        string = "The quick brown fox jumps over the lazy dog \u{1830}\u{1826} \u{1830}\u{1826} \u{1830}\u{1826} \u{1830}\u{1826} \u{1830}\u{1826} The quick brown fox jumps over the lazy dog"
        check = viewController.checkValidTweet(with: string)
        XCTAssertTrue(check, "Tweet is invalid. Tweet must be not a span of nonwhite space character and lenght bigger than 50")
        
        array = viewController.splitTweet(with: string)
        arrayCount = array.count
        for sub in array {
            XCTAssertLessThan(sub.count, 51, "Sub tweet must has lenght must not more than 50")
        }
        
        string = "kakakakakak thi sao bay gio ............................................................  va bay gio moi chuyen da k"
        check = viewController.checkValidTweet(with: string)
        XCTAssertTrue(check, "Tweet is invalid. Tweet must be not a span of nonwhite space character and lenght bigger than 50")
        
        array = viewController.splitTweet(with: string)
        arrayCount = array.count
        for sub in array {
            XCTAssertLessThan(sub.count, 51, "Sub tweet must has lenght must not more than 50")
        }
    }
    
    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }
    
}
