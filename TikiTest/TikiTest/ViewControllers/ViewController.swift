//
//  ViewController.swift
//  TikiTest
//
//  Created by THAI LE QUANG on 7/19/18.
//  Copyright © 2018 THAI LE QUANG. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    //MARK: - Outlet
    @IBOutlet weak var tweetTf: UITextField!
    @IBOutlet weak var tweetButton: UIButton!
    @IBOutlet weak var contentTableView: UITableView!
    
    //MARK: - Variable
    private var arrayTweet: [String] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        contentTableView.rowHeight = UITableViewAutomaticDimension
        contentTableView.estimatedRowHeight = 60
        
        contentTableView.isHidden = true
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func tapGesture_clicked(_ sender: Any) {
        self.view.endEditing(true)
    }
    
    @IBAction func tweetTextField_editingChanged(_ sender: Any) {
        if tweetTf.text != "" {
            tweetButton.isEnabled = true
        } else {
            tweetButton.isEnabled = false
        }
    }
    
    @IBAction func tweetButton_clicked(_ sender: Any) {
        self.view.endEditing(true)
        
        let tweet = tweetTf.text?.trimmingCharacters(in: .whitespacesAndNewlines) ?? ""
        if !checkValidTweet(with: tweet) {
            contentTableView.isHidden = true
            showMessage(with: "Tweet is invalid. Tweet must be not a span of nonwhite space character and lenght bigger than 50")
        } else {
            contentTableView.isHidden = false
            arrayTweet.removeAll()
            arrayTweet = splitTweet(with: tweet)
            contentTableView.reloadData()
        }
    }
    
    func checkValidTweet(with tweet: String) -> Bool {
        let tweetCount = tweet.count
        
        if !tweet.containsWhitespace && tweetCount > 50 {
            return false
        }
        
        return true
    }
    
    private func showMessage(with message: String) {
        let alertController = UIAlertController(title: "Warning", message: message, preferredStyle: .alert)
        let okAction = UIAlertAction(title: "OK", style: .default, handler: nil)
        alertController.addAction(okAction)
        self.present(alertController, animated: true, completion: nil)
    }
    
    func splitTweet(with tweet: String) -> [String] {
        let regex = try! NSRegularExpression(pattern: "(.{1,46})(\\s+|$)")
        let range = NSRange(tweet.startIndex..., in: tweet)
        let results = regex.matches(in: tweet, options: .anchored, range: range)
            .map { match -> Substring in
                let range = Range(match.range(at: 1), in: tweet)!
                return tweet[range]
        }
        
        var value = [String]()
        let count = results.count
        if count == 1 {
            return [String(results[0])]
        } else {
            for (index, subString) in results.enumerated() {
                value.append("\(index+1)/\(count) " + String(subString))
            }
        }
        
        return value
    }
}

extension ViewController: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrayTweet.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = TweetTableViewCell.createCell(tableView: tableView)
        cell.visulizeCell(with: arrayTweet[indexPath.row])
        
        return cell
    }
}

// MARK: StoryboardProtocol's members
extension ViewController: StoryboardProtocol {
    
    static func bundle() -> Bundle? {
        return nil
    }
    
    static func storyboardName() -> String {
        return "Main"
    }
}
