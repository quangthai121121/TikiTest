//
//  TweetTableViewCell.swift
//  TikiTest
//
//  Created by THAI LE QUANG on 7/19/18.
//  Copyright © 2018 THAI LE QUANG. All rights reserved.
//

import UIKit

class TweetTableViewCell: UITableViewCell, Cell {

    @IBOutlet weak var tweetLb: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        tweetLb.text = ""
    }
    
    func visulizeCell(with tweet: String) {
        let fullString = "\(tweet) \(tweet.count) characters"
        let range = fullString.range(of: "\(tweet.count) characters")?.nsRange
        
        let attString = NSMutableAttributedString(string: fullString)
        attString.addAttributes([NSAttributedStringKey.font: UIFont.systemFont(ofSize: 12.0)], range: range!)
        
        tweetLb.attributedText = attString
    }
}
