//
//  Helpers.swift
//  TikiTest
//
//  Created by THAI LE QUANG on 7/19/18.
//  Copyright © 2018 THAI LE QUANG. All rights reserved.
//

import UIKit

// MARK: Cell extension
public protocol Cell {
    static var identify: String { get }
}

public extension Cell {
    static var identify: String  {
        return "\(self)"
    }
}

public extension Cell where Self: UITableViewCell {
    static func createCell(tableView: UITableView) -> Self {
        return tableView.dequeueReusableCell(withIdentifier: self.identify) as! Self
    }
}

// MARK: Storyboard extension
public protocol StoryboardProtocol {
    static func bundle() -> Bundle?
    static func storyboardName() -> String
}

public extension StoryboardProtocol where Self: UIViewController {
    static func identifier() -> String {
        return "\(self)"
    }
    
    static func controllerFromStoryboard() -> Self? {
        let storyboard = UIStoryboard(name: self.storyboardName(), bundle: self.bundle())
        return storyboard.instantiateViewController(withIdentifier: self.identifier()) as? Self
    }
}

// MARK: String extension
extension String {
    var containsWhitespace : Bool {
        return(self.rangeOfCharacter(from: .whitespacesAndNewlines) != nil)
    }
}

// MARK: Range extension
extension Range where Bound == String.Index {
    var nsRange:NSRange {
        return NSRange(location: self.lowerBound.encodedOffset,
                       length: self.upperBound.encodedOffset -
                        self.lowerBound.encodedOffset)
    }
}
